#!/bin/sh

### The VirtualBox interface during installation: GUI window or no window.
# INTERFACE=gui
INTERFACE=headless

### The base name of the install ISO
ISONAME=CentOS-6.5-i386

### The Sage version
VERSION=8.1

### Number of threads to compile Sage with 
### Note: the finished appliance will use only one CPU core by default
BUILD_THREADS=4


### end of user configurable section
export INTERFACE
export ISONAME
export VERSION
export BUILD_THREADS

PWD=`pwd`
CONFIGDIR=$PWD/configuration
SRCDIR=$PWD/src
DATADIR=$PWD/data
if [ ! -d "$CONFIGDIR" ] || [ ! -d "$SRCDIR" ] || [ ! -e "build.sh" ] ; then
    echo "You must run ./build-sage-virtualbox.sh in its home directory."
    exit 1
fi

if [ ! -e "$SRCDIR/$ISONAME" ] ; then
    echo "You must extract the ISO image to $SRCDIR/$ISONAME"
    exit 1
fi
export CONFIGDIR
export SRCDIR
export DATADIR

SOURCE="src/sage-$VERSION.tar.gz"
if [ ! -f $SOURCE ] ; then
    echo "Cannot find Sage source archive $SOURCE in the $SRCDIR directory."
    exit 1
fi
export SOURCE

rm -rf $DATADIR
mkdir $DATADIR


# build bootable dvd
./build-install-dvd.sh
if [ $? -ne 0 ]; then
   echo "Error - Failed to create boot DVD!"
   exit 1
fi

# Base system install
UUID=`uuidgen`
export UUID
./build-virtualbox.sh
if [ $? -ne 0 ]; then
   echo "Error - Failed to install base system"
   exit 1
fi

# Updated kernel and install guest additions
./build-guest-additions.sh
if [ $? -ne 0 ]; then
   echo "Error - Failed to install guest additions"
   exit 1
fi

# Start VM
./start-vm.sh || exit 1

# Compile Sage and make it auto-start
./build-sample-notebooks.sh || exit 1

# Compile Sage and make it auto-start
./build-sage.sh || exit 1

# Export the virtual appliance
./build-appliance.sh || exit 1



