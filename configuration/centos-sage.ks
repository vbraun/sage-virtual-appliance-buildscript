# Perform the kickstart install in Text Mode
text

#Security
network --bootproto=dhcp --device=eth0 --onboot=yes --hostname sagevm --activate
firewall --disable
selinux --enforcing
auth --useshadow --passalgo=sha512

#rootpw --lock sage
rootpw sage

group --name=vboxsf
user --name sage --password sage --groups vboxsf

#Language
keyboard us
lang en_US.UTF-8
timezone  US/Eastern

#Boot
zerombr
bootloader --location=mbr --timeout=3 --append="rhgb quiet biosdevname=0"


#Partitions
clearpart --all --initlabel
part biosboot --fstype=biosboot --size=1
part /		--fstype ext3 --ondisk sda --size 1 --grow
part swap	--fstype swap --ondisk sda --size 1024

# YUM Repository for Chrome/Chromium
repo --name=centos-chromium --baseurl=http://people.centos.org/hughesjr/chromium/6/i386
poweroff



%packages
@core
kernel-devel
kernel-headers
make
atlas
atlas-devel
openssl-devel
pango-devel
cairo-devel
readline-devel
dejavu-sans-fonts
dejavu-serif-fonts
dejavu-sans-mono-fonts
-NetworkManager
-NetworkManager-glib
yum-cron
chromium
@X Window System --nodefaults
mesa-dri-drivers
libXcomposite
libXdamage
gtk2
alsa-lib
icedtea-web
ImageMagick
vlgothic-fonts
git
gcc
gcc-c++
gcc-gfortran
libgfortran
perl-ExtUtils-MakeMaker
python
patch

### Pandoc is not packaged on Centos
# pandoc

%end


%post --log=/root/kickstart-post.log

rm -f /etc/sysconfig/network-scripts/ifcfg-p*
rm -f /etc/udev/rules.d/70-persistent-net.rules
rm -f /lib/udev/rules.d/75-persistent-net-generator.rules

# Make plymouth happy so we can boot
# /usr/sbin/plymouth-set-default-plugin text
/usr/sbin/plymouth-set-default-theme solar

#turn on services
/sbin/chkconfig --level=3 network on
/sbin/chkconfig --level=3 crond on
/sbin/chkconfig --level=3 sshd on

# turn off stuff thats not necessary
/sbin/chkconfig --level=3 NetworkManager off
/sbin/chkconfig --level=3 auditd off
/sbin/chkconfig --level=3 mdmonitor off
/sbin/chkconfig --level=3 netfs off
/sbin/chkconfig --level=3 nfslock off
/sbin/chkconfig --level=3 pcscd off
/sbin/chkconfig --level=3 rpcbind off
/sbin/chkconfig --level=3 rpcgssd off
/sbin/chkconfig --level=3 rpcidmapd off
/sbin/chkconfig --level=3 rsyslogd off
/sbin/chkconfig --level=3 sendmail off
/sbin/chkconfig --level=3 sssd off
/sbin/chkconfig --level=3 abrtd off
/sbin/chkconfig --level=3 abrt-ccpp off
/sbin/chkconfig --level=3 lvm2-monitor off
/sbin/chkconfig --level=3 postfix off


# important: need to turn off firewall or port 8000 will be blocked
# /sbin/chkconfig --level=3 iptables off
# /sbin/chkconfig --level=3 ip6tables off

# Embedd my ssh public key so that the build script can ssh in later
mkdir /root/.ssh
chmod 700 /root/.ssh
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDb7jC7zWmoGS7firnUd9Goor6P/D0bms+J8/utfmFt83Xi2TtGd9YWr1xQ9L9cFc8YaBCRJ5FGJ9HXR0R8upaTappEbcLqQFxUdeB6IQ3Lzpvn31LqT6gb/+CcC0YS3ep2wvv/w1i7A+CQaG70XA0AGmZexDCkMmj2zCJvYn7/u15nqJAVdpXni8+IcvdS3sNnPzid+6SrjX64vBw8NCSDmH43BEGyLV/R9MpStZgRpxXD/E+kWrsgxu/ty0Wd6NwyX/gOMwDgUdk5DY6d43eX4HYRcTuq6kBdhO4vEz6hRjmEymeiuUQt5hYFtvhh/gzg9TYxoNSYHwW4W9WOutcbnQ+czaciWoNKnr/1Hj8+rrk0gkL1wfyiG7e7Z9NoStULLYTOcSbmxv784eMv43ZEnDOG6Fafe6Ichg8mbyAzO6K9mgLk4WuH66x9vtr9kdSFY+pZxc12xhrjtPIPaAsvX7171EDSUWCw63mgOJDHlc9CaTM5SfnVSOFHAfNCqP/WJb6P5/9PBXDBEjLcQqjy5T5yIUyao5XILeFqpzquJunr0pHGYDhkkIrlPwG6L+a1sIf6qEmsFgOpcUTRwrbCz5CI7eVNAV2KPJZCnfRwfULStohxVvFsfRclgiW4qJIrcx4DS2Hs0gTtJmnP/sGocAncK9DGrOnRFofD/dLtxQ== Volker Braun : vbraun.name@gmail.com : Thinkpad W530 : RSA Fallback > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
restorecon -R /root/.ssh


echo 'sage    ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/10_sage
chmod 0440 /etc/sudoers.d/10_sage


# include signing keys
rpm --import http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-Testing-6


%end
