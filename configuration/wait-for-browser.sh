#!/bin/sh

while true ; do
    echo Waiting for notebook to start up...
    if nc -d -w 0 localhost 8000 ; then
	break
    fi
    sleep 1
done

echo Starting browser
defaultbrowser http://localhost:8000

