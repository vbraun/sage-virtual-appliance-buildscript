# Perform the kickstart install in Text Mode.  Installs are
# performed in graphical mode by default.
text

#Security
network --bootproto=dhcp --device=eth0 --onboot=yes --hostname sagevm --activate
firewall --disable
selinux --enforcing
auth --useshadow --passalgo=sha512

#rootpw --lock sage
rootpw sage

group --name=vboxsf
user --name sage --password sage --groups vboxsf

#Language
keyboard us
lang en_US.UTF-8
timezone  US/Eastern

#Boot
zerombr
bootloader --location=mbr --timeout=3 --append="rhgb quiet biosdevname=0"


#Partitions
clearpart --all --initlabel
part biosboot --fstype=biosboot --size=1
part /		--fstype ext3 --ondisk sda --grow
part swap	--fstype swap --ondisk sda --size 1024

#repo --name=google-chrome --baseurl=http://dl.google.com/linux/chrome/rpm/stable/i386
repo --name=fedora-chromium --baseurl=http://repos.fedorapeople.org/repos/spot/chromium/fedora-$releasever/$basearch/

poweroff



%packages
@core
kernel-PAE
kernel-PAE-devel
kernel-headers
make
atlas
atlas-devel
gcc-gfortran
libgfortran
gcc-c++
dkms
openssl-devel
pango-devel
cairo-devel
readline-devel
dejavu-sans-fonts
dejavu-serif-fonts
dejavu-sans-mono-fonts
-NetworkManager
-NetworkManager-glib
chromium
@X Window System --nodefaults
mesa-dri-drivers
libXcomposite
libXdamage
openbox
gtk2
alsa-lib
icedtea-web
ImageMagick
vlgothic-fonts
%end


%post --log=/root/kickstart-post.log

rm -f /etc/sysconfig/network-scripts/ifcfg-p*
rm -f /etc/udev/rules.d/70-persistent-net.rules
rm -f /lib/udev/rules.d/75-persistent-net-generator.rules

# Make plymouth happy so we can boot
# /usr/sbin/plymouth-set-default-plugin text
/usr/sbin/plymouth-set-default-theme solar

#turn on services
/sbin/chkconfig --level=3 network on
/sbin/chkconfig --level=3 crond on
/sbin/chkconfig --level=3 sshd on

# turn off stuff thats not necessary
/sbin/chkconfig --level=3 NetworkManager off
/sbin/chkconfig --level=3 auditd off
/sbin/chkconfig --level=3 mdmonitor off
/sbin/chkconfig --level=3 netfs off
/sbin/chkconfig --level=3 nfslock off
/sbin/chkconfig --level=3 pcscd off
/sbin/chkconfig --level=3 rpcbind off
/sbin/chkconfig --level=3 rpcgssd off
/sbin/chkconfig --level=3 rpcidmapd off
/sbin/chkconfig --level=3 rsyslogd off
/sbin/chkconfig --level=3 sendmail off
/sbin/chkconfig --level=3 sssd off

# important: need to turn off firewall or port 8000 will be blocked
# /sbin/chkconfig --level=3 iptables off
# /sbin/chkconfig --level=3 ip6tables off

mkdir /root/.ssh
chmod 700 /root/.ssh
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCV5yB6irPOI8Tb+7x88f877GQzHzb/SnvLzXzlBolVTEVF70nqR6cvwGSh/m27o+lH4Pz3qj0fsvvaFdOxWs97v4Yab9saxa5pCa8kDjsDhVS5Ym22+96qt3ktxBrMkYzRydgkjl5cSrW6A1QAmJ3iSogul9hXAPPQ4BBwuLHJO02f3rAq2tcQTLdKydOztLyBWK5tIe4yT4vW8jTtliW6b68RYWEv3jpjejENszep337k4Ss305xCrfEbGAdwtZGJMXxSIAnyrrQj6kNtGp3QxGD7ra1dO0dvdGl6S/U0XLRtRqcrXMGyBGihd/66psJBNENQ1ene8TojaFrRAo9/ ThinkPad W520 > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys
restorecon -R /root/.ssh


echo 'sage    ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/10_sage
chmod 0440 /etc/sudoers.d/10_sage

rpm --import https://dl-ssl.google.com/linux/linux_signing_key.pub

%end
