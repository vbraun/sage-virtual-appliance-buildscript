#!/bin/sh



VBoxManage modifyvm $UUID --memory 512 --cpus 1 --hwvirtex off --vram 16
if [ $? -ne 0 ]; then
   echo "Error - Failed to configure for lower hardware requirements!"
   exit 1
fi

rm -f "$DATADIR/sage-$VERSION.ova"
VBoxManage export $UUID --output "$DATADIR/sage-$VERSION.ova" \
    --vsys 0 \
    --product "Sage-$VERSION" \
    --producturl "http://www.sagemath.org" \
    --vendor "The Sage Collaboration" \
    --version "$VERSION"
if [ $? -ne 0 ]; then
   echo "Error - Failed to export Sage appliance!"
   exit 1
fi

chmod ugo+r "$DATADIR/sage-$VERSION.ova"
if [ $? -ne 0 ]; then
   echo "Error - Failed to set permissions for exported Sage appliance!"
   exit 1
fi

VBoxManage unregistervm $UUID --delete
if [ $? -ne 0 ]; then
   echo "Error - Failed to unregister VM!"
   exit 1
fi
echo "Unregistered VM"
