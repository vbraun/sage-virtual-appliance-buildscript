#!/bin/sh



# Start and install virtualbox guest additions
./start-vm.sh || exit 1


# extract guest additions
isoinfo -R -i /usr/share/virtualbox/VBoxGuestAdditions.iso \
    -x /VBoxLinuxAdditions.run > "$DATADIR/"VBoxLinuxAdditions.run

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$DATADIR/"VBoxLinuxAdditions.run root@localhost:/tmp/VBoxLinuxAdditions.run

# Install additional repos
# without dkms kernel updates will disable virtualbox guest additions 

ssh -oNoHostAuthenticationForLocalhost=yes -p2222 root@localhost -T <<EOF
  rpm -ihv https://anorien.csc.warwick.ac.uk/mirrors/epel/6/i386/epel-release-6-8.noarch.rpm
  rpm -ihv http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.i686.rpm
  yum -y install dkms openbox
  yum -y update
  yum clean all
  chmod 755 /tmp/VBoxLinuxAdditions.run
  /tmp/VBoxLinuxAdditions.run
  rm /tmp/VBoxLinuxAdditions.run
  shutdown -h now
EOF


echo "Waiting for the guest addition installation to finish..."
STATUS="State:           running"
while [ "$STATUS" != "" ]; do
    STATUS=`VBoxManage showvminfo $UUID | grep 'State:.*running'`
    date
    sleep 20
done
if [ $? -ne 0 ]; then
   echo "Failed to wait for installation to finish!"
   exit 1
fi

