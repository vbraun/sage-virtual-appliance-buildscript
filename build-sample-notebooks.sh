#!/bin/sh

NBDIR=./worksheets

ssh -oNoHostAuthenticationForLocalhost=yes -p2222 root@localhost -T <<EOF | tee --append "$DATADIR"/install.log
  set -v
  mkdir -p /home/sage/Documents
EOF

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$NBDIR"/* root@localhost:/home/sage/Documents/


