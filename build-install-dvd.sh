#!/bin/sh

if [ -z "$ISONAME" ] || [ -z "$VERSION" ] || [ -z "$INTERFACE" ] ; then
    echo "You should only call this script from build.sh"
    exit 1
fi

if [ ! -f "$SRCDIR/sage-$VERSION.tar.gz" ] ; then
    echo "Sage source file sage-$VERSION.tar.gz does not exist!"
    exit 1
fi

rm -f "$DATADIR"/CentOS-Sage.iso
chmod u+rw "$SRCDIR/$ISONAME"/isolinux/*
cp "$CONFIGDIR"/centos-sage.ks "$SRCDIR/$ISONAME/ks.cfg"
cp "$CONFIGDIR"/centos-isolinux.cfg "$SRCDIR/$ISONAME/isolinux/isolinux.cfg"
if [ $? -ne 0 ]; then
   echo "Error copying files!"
   exit 1
fi

mkisofs -R -r -T -J -v \
    -V "CentOS + Sage Install DVD" \
    -b isolinux/isolinux.bin \
    -c isolinux/boot.cat \
    -no-emul-boot -boot-load-size 4 -boot-info-table \
    -o "$DATADIR"/CentOS-Sage.iso \
    "$SRCDIR/$ISONAME"

