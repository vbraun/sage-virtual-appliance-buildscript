#!/bin/sh

echo "VirtualBox version :"
VBoxManage -v || exit 1
if [ $? -ne 0 ]; then
   echo "Error - Failed to run VBoxManage!"
   exit 1
fi


VBoxManage createvm \
    --name "Sage-$VERSION" \
    --ostype RedHat \
    --basefolder "$DATADIR" \
    --uuid $UUID 
if [ $? -ne 0 ]; then
   echo "Error - Failed to create VM!"
   exit 1
fi
echo "Created new virtual machine with uuid = $UUID"


VBoxManage registervm "$DATADIR/Sage-$VERSION/Sage-$VERSION.vbox"
if [ $? -ne 0 ]; then
   echo "Error - Failed to register VM ... exiting"
   exit 1
fi
echo "Registered new VM"


VBoxManage modifyvm $UUID --memory 4096 --vram 128 --cpus $BUILD_THREADS --hwvirtex on --ioapic on
#VBoxManage modifyvm $UUID --memory 4096 --vram 64 --cpus 1 --hwvirtex off

VBoxManage modifyvm $UUID --acpi on --boot1 dvd
VBoxManage createhd --filename "$DATADIR/Sage-$VERSION.vdi" --size 12000 
VBoxManage storagectl $UUID --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach $UUID --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium "$DATADIR/Sage-$VERSION.vdi"
VBoxManage storagectl $UUID --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach $UUID --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium "$DATADIR/CentOS-Sage.iso"
if [ $? -ne 0 ]; then
   echo "Failed to setup storage for VM!"
   exit 1
fi

# http://www.virtualbox.org/ticket/9029
VBoxManage setextradata $UUID VBoxInternal/PGM/MaxRing3Chunks 4096

#### Old VBox way of setting port forwarding
#VBoxManage setextradata $UUID "VBoxInternal/Devices/e1000/0/LUN#0/Config/sage/Protocol" TCP
#VBoxManage setextradata $UUID "VBoxInternal/Devices/e1000/0/LUN#0/Config/sage/GuestPort" 8000
#VBoxManage setextradata $UUID "VBoxInternal/Devices/e1000/0/LUN#0/Config/sage/HostPort" 8000
### New VirtualBox mechanism for setting port forwarding
VBoxManage modifyvm $UUID --natpf1 "guestsage,tcp,127.0.0.1,8000,,8000"
VBoxManage modifyvm $UUID --natpf1 "guestssh,tcp,127.0.0.1,2222,,22"
if [ $? -ne 0 ]; then
   echo "Failed to set up networking with VM!"
   exit 1
fi

echo "------------------------------------------------------"
VBoxManage showvminfo $UUID
echo "------------------------------------------------------"

# Start and install Fedora + Sage
VBoxManage startvm $UUID --type $INTERFACE
if [ $? -ne 0 ]; then
   echo "Failed to start virtual machine!"
   exit 1
fi

echo "Waiting until the automated installation finishes..."
echo "This should take a long time (hours...)"
STATUS="State:           running"
while [ "$STATUS" != "" ]; do
    STATUS=`VBoxManage showvminfo $UUID | grep 'State:.*running'`
    date
    sleep 60
done
if [ $? -ne 0 ]; then
   echo "Failed to wait for installation to finish!"
   exit 1
fi

VBoxManage storageattach $UUID --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium emptydrive
if [ $? -ne 0 ]; then
   echo "Error - Failed to remove installation medium!"
   exit 1
fi

