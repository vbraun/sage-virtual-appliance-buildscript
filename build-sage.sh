#!/bin/sh


scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$SRCDIR/sage-$VERSION.tar.gz" root@localhost:/home/sage/sage-source.tar.gz

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$CONFIGDIR"/sage-bash-profile root@localhost:/home/sage/.bash_profile

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$CONFIGDIR"/sage-xinitrc root@localhost:/home/sage/.xinitrc

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$CONFIGDIR"/notebook-browser root@localhost:/home/sage/notebook-browser

# Auto-login

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$CONFIGDIR"/centos-start-ttys-override root@localhost:/etc/init/start-ttys.override

scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
    "$CONFIGDIR"/centos-sage-tty root@localhost:/etc/init/sage-tty.conf

# Systemd will be used in the future
#
#scp -oNoHostAuthenticationForLocalhost=yes -P2222 \
#    "$CONFIGDIR"/systemd-run-sage.service root@localhost:/lib/systemd/system/sage@.service
#
#  cd /etc/systemd/system/getty.target.wants
#  ln -sf /lib/systemd/system/sage@.service getty@tty8.service


ssh -oNoHostAuthenticationForLocalhost=yes -p2222 root@localhost -T <<EOF | tee  "$DATADIR"/install.log
  set -v
  chown -R sage.sage /home/sage/

  su sage
  set -v
  cd
  tar xf sage-source.tar.gz
  rm -f sage-source.tar.gz
  ln -s sage* sage
  cd sage
  # export SAGE_PARALLEL_SPKG_BUILD=yes
  # export SAGE_ATLAS_ARCH=fast
  export SAGE_ATLAS_LIB=/usr/lib/atlas
  export SAGE_FAT_BINARY="yes"
  export MAKE='make -j$BUILD_THREADS'
  make
  make ptest
  ./sage -pip install terminado
  ./sage <<EOFSAGE
    from sage.misc.misc import DOT_SAGE
    from sagenb.notebook import notebook
    directory = DOT_SAGE+'sage_notebook'
    nb = notebook.load_notebook(directory)
    nb.user_manager().add_user('admin', 'sage', '', force=True)
    nb.save()
    quit
EOFSAGE
EOF

RC=`grep "Error building Sage" "$DATADIR"/install.log` 
if [ "$RC" != "" ]; then
   echo "Error building Sage!"
   VBoxManage unregistervm $UUID --delete
   exit 1
fi

sleep 5
ssh -oNoHostAuthenticationForLocalhost=yes -p2222 root@localhost -T <<EOF
  dd if=/dev/zero of=/zerofile ; rm -f /zerofile
  shutdown -h now
EOF


echo "Waiting for the Sage installation to finish..."
STATUS="State:           running"
while [ "$STATUS" != "" ]; do
    STATUS=`VBoxManage showvminfo $UUID | grep 'State:.*running'`
    date
    sleep 20
done
if [ $? -ne 0 ]; then
   echo "Failed to wait for installation to finish!"
   exit 1
fi


