#!/bin/sh


# Start the Virtual Machine
VBoxManage startvm $UUID --type $INTERFACE
if [ $? -ne 0 ]; then
   echo "Failed to start virtual machine!"
   exit 1
fi


STATUS="booting"
while [ "$STATUS" != "ready" ]; do
    echo "Waiting for VM to start."
    sleep 10
    STATUS=`ssh -oNoHostAuthenticationForLocalhost=yes -p2222 root@localhost echo "ready"`
done

